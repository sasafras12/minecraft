#pragma once
#include <fstream>
#include <vector>
#include "Utils.h"

class TGAImage {
public:
	TGAImage(int w, int h, std::string file);
	~TGAImage();
private:
	int width; int height;
	std::string _file;
	std::ofstream out;
	std::string filename = "./build/" + _file + ".tga";
	int channels = 3;
	unsigned long npixels = width * height;
	unsigned long nbytes = npixels * channels;
	unsigned char* data;

	unsigned char red[3] = {0, 0, 255};
	unsigned char yellow[3] = {0, 255, 255};
	unsigned char white[3] = {255, 255, 255};

public:
	void drawPixel(int x, int y, unsigned char* col);
	void drawLine(int x0, int y0, int x1, int y1, unsigned char* col);
	void drawTriangle(vec2i v1, vec2i v2, vec2i v3, unsigned char* col);
	vec2i projectedPoint(vec3f v);
	void writeHeader();
	void unload_rle_data();
};
