#pragma once
template<  int i, typename T >
class vec {
public:
	vec<i, T>(){};
	vec<i, T>(T t1, T t2) : x(t1), y(t2){};
	vec<i,T>(T t1, T t2, T t3) : x(t1), y(t2), z(t3){}
	vec<i,T>& operator=(const std::vector<double> &v){ x = v[0]; y = v[1]; z = v[2]; return *this; };
	bool operator < (const vec<i,T>& v) const { return (y < v.y); }
public:
	T x, y, z;
	T *v1 = &x;
	T *v2 = &y;
	T *v3 = &z;
};

typedef vec<2, float> vec2f;
typedef vec<2, int> vec2i;
typedef vec<3, float> vec3f;
typedef vec<3, int> vec3i;
typedef vec<3, double> vec3d;

