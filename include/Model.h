#pragma once
#include <vector>
#include <string>
#include "Utils.h"

class Model {
public:
	Model();
	~Model();
	int nfaces();
	std::vector<std::vector<double> > face( int index );
	void loadModelFromObj(std::string path);
	std::vector<std::vector<vec3i > > faces;
	std::vector<vec3f> verts;
};
