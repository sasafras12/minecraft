main: ./src/main.cpp ./src/TGAImage.cpp ./src/Model.cpp
	# making TGAImage.o...
	g++ ./src/TGAImage.cpp -I./include -Wall -g -c -o ./obj/TGAImage.o
	# making Model.o...
	g++ ./src/Model.cpp -I./include -Wall -g -c -o ./obj/Model.o
	# linking to main...
	g++ ./src/main.cpp ./obj/Model.o ./obj/TGAImage.o -I./include -L./obj -Wall -g -o ./bin/main

run:
	./bin/main ${ARGS}

clean:
	# removing .obj files...
	rm ./obj/*.o
	# removing main...
	rm ./bin/main
	# removing .tga files...
	rm ./build/*.tga

test: ./test/test.cpp
	# testing...
	g++ ./test/test.cpp -I./include -Wall -g -o ./test/test

.PHONY: test
