using System.Numerics;
public class PolynomialArray
{
	// instance variables
	private BigInteger[] poly;
	private int degree;
	private int arraySize;
	private static final int DEFAULT_SIZE = 1000000;

	// constructor
	public PolynomialArray()
	{
		arraySize = DEFAULT_SIZE;
		poly = new BigInteger[arraySize];
		degree = 0;
	}
	// constructor that takes size as input
	public PolynomialArray(int size)
	{
		poly = new BigInteger[size];
		degree = 0;
		arraySize = size;
	}
	// constructor that takes data as input
	public PolynomialArray(int[] data)
	{
		poly = new BigInteger[DEFAULT_SIZE];

		for (int ctr = 1; ctr < data.length; ctr = ctr + 2)
		{
			if (data[ctr] > degree)
			{
				degree = data[ctr];	
			}
			poly[data[ctr]] = new BigIntExtended(data[ctr - 1]);
		}
	}
	// constructor that takes size and data as inputs
	public PolynomialArray(BigInteger[] data, int deg)
	{
		poly = data;
		degree = deg;
	}

	// this method returns a String representation of the object
	public String ToString()
	{
		String outputString = "";
		if (degree != 0)
		{
			for (int i = 0; i <= degree; i++)
			{
				if (poly[i] != null && !poly[i].equals(BigInteger.ZERO))
				{
					outputString += "" + poly[i] + "*x^" + i + " + ";
				}
			}
		} else {
			outputString += "0";
		}
		return outputString;
	}
	// this method adds two PolynomialArrays
	public void Add(PolynomialArray p)
	{
		if (p.getDegree() > degree)
		{
			degree = p.getDegree();
			BigInteger[] temp = new BigInteger[degree + 1];

			for (int d = 0; d <= degree; d++)
			{
				if (!this.getCoeffAtIndex(d).equals(BigIntExtended.ZERO))
				{
					if (temp[d] != null)
					{
						temp[d] = temp[d].Add(this.getCoeffAtIndex(d));
					} else {
						temp[d] = this.getCoeffAtIndex(d);
					}
				}
			}
			for (int c = 0; c <= degree; c++)
			{
				if (!p.getCoeffAtIndex(c).equals(BigIntExtended.ZERO))
				{
					temp[c] = p.getCoeffAtIndex(c);
				}
			}

			poly = temp;
		}

		for (int c = 0; c <= p.getDegree(); c++)
		{
			if (!p.getCoeffAtIndex(c).equals(BigIntExtended.ZERO))	
			{
				this.addCoeffAtIndex(c, p.getCoeffAtIndex(c));
			}
		}
	}
	// this method subtracts p from this polynomial
	public void subtract(PolynomialArray p)
	{
		p.multiplyByConstant(-1);
		this.Add(p);
	}
	// this method multiplies the coefficients of this polynomial
	// by a constant c
	public void multiplyByConstant(int c)
	{
		for (int p = 0; p <= degree; p++)
		{
			if (poly[p] != null)
			{
				poly[p] = poly[p].multiply(new BigIntExtended(c));
			}
		}
	}
	// makes this polynomial into a polynomial
	// representing (x+a)^n, using an algorithm of
	// binomial expansion
	public void binomialExpansion(int a, int n)
	{
		poly = new BigInteger[n+1];
		BigInteger temp = new BigIntExtended("0");
		BigInteger bigA = new BigIntExtended(a);
	
		for (int k = 0; k <= n; k++)
		{
			temp = factorial(n);
			temp = temp.divide(factorial(n-k).multiply(factorial(k)));

			bigA = new BigIntExtended(a);
			bigA = bigA.pow(k);

			poly[n - k] = temp.multiply(bigA);
		}
		degree = n;
	}
	// makes this Polynomial into a polynomial
	// representing (x+a)^n, using an algorithm of
	// successive squaring
	public void binomialExpansionViaSquaring(int a, int n)
	{
		poly = new BigInteger[n+1];
		BigInteger[] tempPoly = new BigInteger[n + 1];
		tempPoly[0] = new BigIntExtended(a);
		tempPoly[1] = new BigIntExtended(1);
		int tempDegree = 1;
		
		BigInteger[] newPoly = new BigInteger[n + 1];
		newPoly[0] = new BigIntExtended(a);
		newPoly[1] = new BigIntExtended(1);
		int newDegree = 1;

		String binaryN = Integer.toBinaryString(n);

		char c;

		tempPoly = multiply(tempPoly, tempDegree, tempPoly, tempDegree);
		tempDegree *= 2;

		for (int index = 1; index < binaryN.length(); index++)
		{
			c = binaryN.charAt(index);
			if (c == '1')
			{
				tempPoly = multiply(tempPoly, tempDegree, newPoly, newDegree);
				tempDegree++;			
			}

			if (index != binaryN.length() -1 )	
			{
				tempPoly = multiply(tempPoly, tempDegree, tempPoly, tempDegree);
				tempDegree *= 2;
			}
		}
		poly = tempPoly;
		degree = tempDegree;
	}

	// this private helper method multiplies two polynomials
	private BigInteger[] multiply(BigInteger[] bi1, int degree1, BigInteger[] bi2, int degree2)
	{
		BigInteger[] polya = bi1;
		BigInteger[] polyb = bi2;
		int newDegree = degree1 + degree2;

		BigInteger[] polytemp = new BigInteger[newDegree + 1];

		for (int c = 0; c <= degree1; c++)
		{
			for (int d = 0; d <= degree2; d++)
			{
				if (polya[c] != null && polyb[d] != null)
				{
					BigInteger t = polya[c].multiply(polyb[d]);

					if (polytemp[c+d] != null)
					{
						polytemp[c+d] = polytemp[c+d].Add(t);	
					} else {
						polytemp[c+d] = new BigInteger("0");
						polytemp[c+d] = polytemp[c+d].Add(t);
					}
				}
			}
		}
		return polytemp;
	}

	// this method checks to see if this Polynomial
	//is congruent to zero mod (x^r -1, n)
	public bool specialMod(int r, int n)
	{
		BigInteger total = new BigIntExtended("0");
		bool mod = true;

		for (int start = 0; start < r; start++)
		{
			total = new BigIntExtended("0");
			for (int x = start; x < this.getDegree(); x = x + r)
			{
				BigInteger temp = poly[x];
				total = total.Add(temp);
			}

			BigIntExtended bigN = new BigIntExtended(n);

			if (!total.mod(bigN).equals(BigIntExtended.ZERO)) {
				mod = false;
			}
		}
		return mod;
	}

}
