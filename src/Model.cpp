#include "Model.h"
#include <iostream>
#include <fstream>
#include <string>

Model::Model()
{

}

Model::~Model()
{

}
void Model::loadModelFromObj(std::string path)
{
	std::ifstream in(path);
	std::string line;

	vec3f vert;
	std::vector<vec3f> _verts;

	vec3i face;
	std::vector<std::vector<vec3i> > _faces;

	std::vector<vec3i> f;
	int found;

	if (in.is_open())
	{
		while ( getline(in, line))
		{
			if (!line.compare(0, 2, "f ")) {
				
				std::string s = line.substr(2, line.length()-1);
				std::string bracket = "/";
				std::string space = " ";

				f.clear();
				for (int i = 0; i < 3; i++)
				{
					// splits each line by bracket and space
					found = s.find(bracket);
					face.x = std::stof(s.substr(0, s.find(bracket)));
					s = s.substr(found+1, s.length()-1);

					found = s.find(bracket);
					face.y = std::stof(s.substr(0, s.find(bracket)));
					s = s.substr(found+1, s.length()-1);

					found = s.find(bracket);
					face.z = std::stof(s.substr(0, s.find(bracket)));

					found = s.find(space);
					s = s.substr(found+1, s.length()-1);
					f.push_back(face);
				}
				_faces.push_back(f);
			}
			else if (!line.compare(0, 2, "v "))
			{
				vec3f vert;
				std::string s = line;
				std::string space = " ";
				int found = s.find(space);
				s = s.substr(found+1, s.length()-1);
				vert.x = std::stof(s.substr(0, s.find(space)));

				found = s.find(space);
				s = s.substr(found+1, s.length()-1);
				vert.y = std::stof(s.substr(0, s.find(space)));

				found = s.find(space);
				s = s.substr(found+1, s.length()-1);
				vert.z = std::stof(s.substr(0, s.find(space)));
				_verts.push_back(vert);
			}

		}
	}
	verts = _verts;
	faces = _faces;
	/*for (auto vert : _verts)
	{
		std::cout << vert.x << " " << vert.y << " " << vert.z << std::endl;
	}
	for (auto face : _faces)
	{
		for (auto verts : face)
		{
			std::cout << *verts.v1 << " " << *verts.v2 << " " << *verts.v3 << std::endl;
		}
		
	}*/
}
