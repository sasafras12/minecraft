#include <iostream>
#include <algorithm>
#include <gtk/gtk.h>

int width = 500, height = 500;

GtkImage *image;
GdkPixbuf *pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, width, height);
guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
int rowstride = gdk_pixbuf_get_rowstride(pixbuf);

void drawPixel(int x, int y, int r, int g, int b)
{
	*(pixels + y * rowstride + x * 3    ) = r;
	*(pixels + y * rowstride + x * 3 + 1) = g;
	*(pixels + y * rowstride + x * 3 + 2) = b;
}

void drawLine(int x1, int y1, int x2, int y2, int r, int g, int b)
	{
		int x, y, dx, dy, dx1, dy1, px, py, xe, ye, i;
		dx = x2 - x1; dy = y2 - y1;
		dx1 = abs(dx); dy1 = abs(dy);
		px = 2 * dy1 - dx1;	py = 2 * dx1 - dy1;
		if (dy1 <= dx1)
		{
			if (dx >= 0)
				{ x = x1; y = y1; xe = x2; }
			else
				{ x = x2; y = y2; xe = x1;}

			drawPixel(x, y, r, g, b);
			
			for (i = 0; x<xe; i++)
			{
				x = x + 1;
				if (px<0)
					px = px + 2 * dy1;
				else
				{
					if ((dx<0 && dy<0) || (dx>0 && dy>0)) y = y + 1; else y = y - 1;
					px = px + 2 * (dy1 - dx1);
				}
				drawPixel(x, y, r, g, b);
			}
		}
		else
		{
			if (dy >= 0)
				{ x = x1; y = y1; ye = y2; }
			else
				{ x = x2; y = y2; ye = y1; }

			drawPixel(x, y, r, g, b);

			for (i = 0; y<ye; i++)
			{
				y = y + 1;
				if (py <= 0)
					py = py + 2 * dx1;
				else
				{
					if ((dx<0 && dy<0) || (dx>0 && dy>0)) x = x + 1; else x = x - 1;
					py = py + 2 * (dx1 - dy1);
				}
				drawPixel(x, y, r, g, b);
			}
		}
}
	

void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int r, int g, int b)
{
	drawLine(x1, y1, x2, y2, r, g, b);
	drawLine(x2, y2, x3, y3, r, g, b);
	drawLine(x3, y3, x1, y1, r, g, b);
	
}

gboolean update(GtkWidget *widget, GdkFrameClock *clock, gpointer user_data)
{
	gdk_pixbuf_fill(pixbuf, 0);
	drawTriangle(100, 20, 20, 100, 150, 150, 255, 255, 255);
	gtk_image_set_from_pixbuf(image, pixbuf);
	return 1;
}

int main()
{
	gtk_init(NULL, NULL);
	GtkWidget *window, *box;
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	image = GTK_IMAGE(gtk_image_new());
	gtk_box_pack_start(GTK_BOX(box), GTK_WIDGET(image), TRUE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(window), box);
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	gtk_widget_add_tick_callback(window, update, NULL, NULL);
	gtk_widget_show_all(window);	
	gtk_main();
}
