#ifndef _VEC_H
#define _VEC_H

#include <math.h>

class vec {


public:
	double x;
	double y;
	double z;
	vec();
	vec(double, double, double);

	// method functions
	double getVecX() { return x; }
	double getVecY() { return y; }
	double getVecZ() { return z; }

	double magnitude () {
		return sqrt((x*x) + (y*y) + (z*z));
	}	
		
	vec normalize () {
		double magnitude = sqrt((x*x) + (y*y) + (z*z));
		return vec(x/magnitude, y/magnitude, z/magnitude);
	}

	vec negative () {
		return vec (-x, -y, -z);
	}

	double dotProduct(vec v) {
		return x * v.getVecX() + y * v.getVecY() + z * v.getVecZ();
	}

	vec crossProduct(vec v) {
		return vec(y*v.getVecZ() - z*v.getVecY(), z*v.getVecX() - x * v.getVecZ(), x*v.getVecY() - y * v.getVecX());
	}

	vec vecAdd(vec v) {
		return vec(x + v.getVecX(), y + v.getVecY(), z + v.getVecZ());
	}

	vec vecMul(double scalar) {
		return vec(x*scalar, y*scalar, z*scalar);
	}

	vec vecSub(vec v) {
		return vec(x - v.getVecX(), y - v.getVecY(), z - v.getVecZ());
	}
};

vec::vec () {
	x = y = z = 0;
}

vec::vec (double i, double j, double k) {
	x = i; y = j; z = k;
}

#endif
