#ifndef _RAY_H
#define _RAY_H

#include <math.h>
#include "vec.h"

class Ray {
	vec origin;
	vec direction;

public:
	Ray();
	Ray(vec, vec);

	// method functions
	vec getRayOrigin() { return origin; }
	vec getRayDirection() { return direction; }

};

Ray::Ray() {
	origin = vec(0, 0, 0);
	direction = vec(1, 0, 0);
}

Ray::Ray (vec o, vec d) {
	origin = o;
	direction = d;
}

#endif
