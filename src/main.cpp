#include <iostream>
#include <string>
#include <vector>
#include "TGAImage.h"
#include "Model.h"

int main(int argc, char** argv) {
	// setup image
	int width = 200, height = 200;
	std::string objFile = (std::string)argv[1];
	TGAImage image(width, height, objFile);

	// define some colours
	unsigned char white[3] = {255, 255, 255};
	unsigned char red[3] = {0, 0, 255};
	unsigned char yellow[3] = {0, 255, 255};
	unsigned char blue[3] = {255, 0, 0};

	std::string filename = "./res/" + objFile + ".obj";

	// create an model object and load model from and .obj file
	Model model;
	model.loadModelFromObj(filename);

	/*for (auto face : model.faces)
	{
			// get coordinates for vertices from face and index into model.verts for corresponding vertex.

			vec3f v1 = model.verts[face[0].x - 1];
			vec3f v2 = model.verts[face[1].x - 1];
			vec3f v3 = model.verts[face[2].x - 1];

			// project points into screen space (formula derived in index.html)

			vec2i pv1 = image.projectedPoint(v1);
			vec2i pv2 = image.projectedPoint(v2);
			vec2i pv3 = image.projectedPoint(v3);

			// draw line between each point onto screen
			image.drawTriangle(pv1, pv2, pv3, red);
	}*/
	image.drawTriangle(vec2i(10,50), vec2i(0,0), vec2i(150,150), red);

    return 0;
}






