// https://developer.gnome.org/gtk3/stable/gtk-getting-started.html
// minimal example
#include <gtk/gtk.h>

int main()
{	gtk_init(NULL, NULL);
	GtkWidget *window, *box;
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_container_add(GTK_CONTAINER(window), box);
	gtk_widget_show_all(GTK_WIDGET(window));
	GdkWindow *win = gtk_widget_get_window(GTK_WIDGET(window));
	GdkScreen *screen = gdk_window_get_screen(win);
	GdkDisplay *display = gdk_window_get_display(win);
	gdk_display_warp_pointer(display, screen, 100, 0);
	gtk_main();
}
