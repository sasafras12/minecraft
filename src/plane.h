#ifndef _PLANE_H
#define _PLANE_H

#include <math.h>
#include "object.h"
#include "vec.h"
#include "colour.h"
#include "ray.h"

class Plane : public Object {
	vec normal;
	double distance;
	Colour colour;

public:
	Plane();
	Plane(vec, double, Colour);

	// method functions
	vec getPlaneNormal() { return normal; }
	double getPlaneDistance() { return distance; };
	virtual Colour getColour() { return colour; }

	virtual vec getNormalAt(vec point) {
		return normal;
	}

	virtual double findIntersection(Ray ray) {
		vec ray_direction = ray.getRayDirection();
		double a = ray_direction.dotProduct(normal);
		
		if (a == 0) {
			// ray is parallel to the plane
			return -1;
		} else {
			double b = normal.dotProduct(ray.getRayOrigin().vecAdd(normal.vecMul(distance).negative()));

			return -1 * b/a;
		}
	}

};

Plane::Plane() {
	normal = vec(1, 0, 0);
	distance = 0;
	colour = Colour(0.5, 0.5, 0.5, 0);
}

Plane::Plane(vec normalValue, double distanceValue, Colour colourValue) {
	normal = normalValue;
	distance = distanceValue;
	colour = colourValue;
}

#endif
