#include <iostream>
#include <map>

int main()
{
	int width = 100;
	int height = 100;

	// move camera to light

	std::map<std::pair<int, int,int>, int> lightSpaceBuffer;
	for (int x = 0; x < width; i++)
	{
		for int y = 0; y < height; y++)
		{
			// calculate z buffer

			// convert lightspace position to world space position
			
			// store position and z buffer
			lightSpaceBuffer[std::make_tuple(world_x, world_y, world_z)] = z_buffer;
		}	
	}

	// move camera back

	for (int x = 0; x < width; i++)
	{
		for int y = 0; y < height; y++)
		{
			// convert viewspace position at x,y to world space position

			// query map for light z buffer
			std::cout << lightSpaceBuffer[std::make_tuple(world_x, world_y, world_z)] << std::endl;

			// compare distance from world space point to light with z buffer
			// draw shadow if distance is longer than z buffer

	return 0;
}
