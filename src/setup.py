from distutils.core import setup
from distutils.extension import Extension

# run : python setup.py build_ext --inplace

ext_instance = Extension(
    'GTKGameEngine',
    sources=['gtk_bindings.cpp'],
    libraries=['boost_python', 'gtk-3', 'gdk-3', 'pangocairo-1.0', 'pango-1.0', 'atk-1.0', 'cairo-gobject', 'cairo', 'gdk_pixbuf-2.0', 'gio-2.0', 'gobject-2.0', 'glib-2.0'],
    extra_compile_args = ["-pthread", "-I/usr/include/gtk-3.0", "-I/usr/include/at-spi2-atk/2.0", "-I/usr/include/at-spi-2.0", "-I/usr/include/dbus-1.0", "-I/usr/lib/x86_64-linux-gnu/dbus-1.0/include", "-I/usr/include/gtk-3.0", "-I/usr/include/gio-unix-2.0/", "-I/usr/include/cairo", "-I/usr/include/pango-1.0", "-I/usr/include/harfbuzz", "-I/usr/include/pango-1.0", "-I/usr/include/atk-1.0", "-I/usr/include/cairo", "-I/usr/include/pixman-1", "-I/usr/include/freetype2", "-I/usr/include/libpng16", "-I/usr/include/freetype2", "-I/usr/include/libpng16", "-I/usr/include/gdk-pixbuf-2.0", "-I/usr/include/libpng16", "-I/usr/include/glib-2.0", "-I/usr/lib/x86_64-linux-gnu/glib-2.0/include", "-std=c++17"],
)

setup(
    name='GTKGameEngine',
    version='0.1',
    ext_modules=[ext_instance])
