#ifndef _SPHERE_H
#define _SPHERE_H

#include <math.h>
#include "object.h"
#include "vec.h"
#include "colour.h"

class Sphere : public Object {
	vec centre;
	double radius;
	Colour colour;

public:
	Sphere();
	Sphere(vec, double, Colour);

	// method functions
	vec getSphereCentre() { return centre; }
	double getSphereRadius() { return radius; };
	virtual Colour getColour() { return colour; }
	virtual vec getNormalAt(vec point) {
		// normal always points away from the centre of a sphere
		vec normal_vec = point.vecAdd(centre.negative()).normalize();
		return normal_vec;	
	}
	virtual double findIntersection(Ray ray) {
		vec ray_origin = ray.getRayOrigin();
		double ray_origin_x = ray_origin.getVecX();
		double ray_origin_y = ray_origin.getVecY();
		double ray_origin_z = ray_origin.getVecZ();

		vec ray_direction = ray.getRayDirection();
		double ray_direction_x = ray_direction.getVecX();
		double ray_direction_y = ray_direction.getVecY();
		double ray_direction_z = ray_direction.getVecZ();

		vec sphere_centre = centre;
		double sphere_centre_x = sphere_centre.getVecX();
		double sphere_centre_y = sphere_centre.getVecY();
		double sphere_centre_z = sphere_centre.getVecZ();
		
		double a = 1; // normalized
		double b = 2 * (ray_origin_x - sphere_centre_x) * ray_direction_x + 2 * (ray_origin_y - sphere_centre_y) * ray_direction_y + 2 * (ray_origin_z - sphere_centre_z) * ray_direction_z;
		double c = pow(ray_origin_x - sphere_centre_x, 2) + pow(ray_origin_y - sphere_centre_y, 2) + pow(ray_origin_z - sphere_centre_z, 2) - (radius * radius);

		double discriminant = b*b - 4 * c;

		if (discriminant > 0) {
			// the ray intersects the sphere
			
			// the first root
			double root_1 = ((-1*b - sqrt(discriminant))/2) - 0.000001;
			if (root_1 > 0) {
				// the first root is the smallest positive root
				return root_1;
			} else {
				// second root is the smallest psoitive root
				double root_2 = ((sqrt(discriminant) - b)/2) - 0.000001;
				return root_2;
			}
		} else {
			// the ray missed the sphere
			return -1;
		}

	}

};

Sphere::Sphere() {
	centre = vec(0, 0, 0);
	radius = 1.0;
	colour = Colour(0.5, 0.5, 0.5, 0);
}

Sphere::Sphere(vec centreValue, double radiusValue, Colour colourValue) {
	centre = centreValue;
	radius = radiusValue;
	colour = colourValue;
}

#endif
