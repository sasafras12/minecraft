#include <iostream>
#include <math.h>
#include "TGAImage.h"
#include <algorithm>
#include <stdio.h>
TGAImage::TGAImage(int w, int h, std::string file): width(w), height(h), _file(file)
{
	data = new unsigned char[nbytes];
	std::fill_n(data, nbytes, 0);
	out.open(filename, std::ios::binary);
	writeHeader();
};

TGAImage::~TGAImage()
{
	unload_rle_data();
	// not sure how unload rle data actually works, when encoded with utf8 theres alot of 255 regardless of the colour
	// which could meand
	//out.write((char *)data, width*height*channels);
	out.close();
	delete[] data;
};

void TGAImage::drawPixel(int x, int y, unsigned char *raw)
{
	std::copy(raw, raw + channels, data + (x+y*width) * channels);
}

void TGAImage::drawLine(int x1, int y1, int x2, int y2, unsigned char* col)
{	
	int x, y, dx, dy, dx1, dy1, px, py, xe, ye, i;
	dx = x2 - x1; dy = y2 - y1;
	dx1 = abs(dx); dy1 = abs(dy);
	px = 2 * dy1 - dx1;	py = 2 * dx1 - dy1;
	if (dy1 <= dx1)
	{
		if (dx >= 0)
			{ x = x1; y = y1; xe = x2; }
		else
			{ x = x2; y = y2; xe = x1;}

		drawPixel(x, y, col);
		
		for (i = 0; x<xe; i++)
		{
			x = x + 1;
			if (px<0)
				px = px + 2 * dy1;
			else
			{
				if ((dx<0 && dy<0) || (dx>0 && dy>0)) y = y + 1; else y = y - 1;
				px = px + 2 * (dy1 - dx1);
			}
			drawPixel(x, y, col);
		}
	}
	else
	{
		if (dy >= 0)
			{ x = x1; y = y1; ye = y2; }
		else
			{ x = x2; y = y2; ye = y1; }

		drawPixel(x, y, col);

		for (i = 0; y<ye; i++)
		{
			y = y + 1;
			if (py <= 0)
				py = py + 2 * dx1;
			else
			{
				if ((dx<0 && dy<0) || (dx>0 && dy>0)) x = x + 1; else x = x - 1;
				py = py + 2 * (dx1 - dy1);
			}
			drawPixel(x, y, col);
		}
	}
}

void triangle(int x1, int y1, int x2, int y2, int x3, int y3, int r, int g, int b) { 
    // sort the vertices, t0, t1, t2 lower−to−upper (bubblesort yay!) 
    if (y1 > y2)
    {
		std::swap(x1, x2);
		std::swap(y1, y2);
	}
    if (y1 > y3)
    {
		std::swap(x1, x3);
		std::swap(y1, y3);
	}
    if (y2 > y3)
    {
		std::swap(x2, x3);
		std::swap(y2, y3);
	}

    int total_height = y3 - y1;
 
    for (int y = y1; y <= y2; y++) {
		//drawLine(y, 

    } 
    for (int y = y2; y <= y3; y++) { 


    } 
}



int pointLine(int x1, int y1, int x2, int y2, int y)
{
	float m = (y2 - y1) / (x2 - x1);
	float b = y - m * x1;
	int x = (y - b) / x1;

	return x;
}

void TGAImage::drawTriangle(vec2i v1, vec2i v2, vec2i v3, unsigned char* col)
{
	drawLine(v1.x, v1.y, v2.x, v2.y, col);
	drawLine(v2.x, v2.y, v3.x, v3.y, col);
	drawLine(v1.x, v1.y, v3.x, v3.y, col);

	// split into two triangles
	// sort verts
	// draw lines horizontally
	// profit

	std::vector<vec2i> vals = {v1, v2, v3};

	std::sort( vals.begin(), vals.end());
	int x1;
	int x2;

	for (int i=vals[0].y; i<vals[1].y; i++)
	{

		x1 = pointLine(v1.x, v1.y, v2.x, v2.y, i);
		x2 = pointLine(v1.x, v1.y, v3.x, v3.y, i);
		drawLine(x1, i, x2, i, col);
	}



}

vec2i TGAImage::projectedPoint(vec3f v) {
	int depth = 2;
	v.x = width * ( ( v.x * depth / (2 * (v.z + depth)) ) + 0.5);
	v.y = height * ( ( v.y * depth / (2 * (v.z + depth)) ) + 0.5);
	vec2i pv(v.x, v.y);
	return pv;
}

void TGAImage::writeHeader()
{	
	char header[18] = { 0 };
	header[2] = 10;
	header[12] = width;
	header[14] = height;
	header[16] = channels<<3;
	out.write(header, sizeof(header));
}

void TGAImage::unload_rle_data()
{
	const unsigned char max_chunk_length = 128;
	unsigned char run_length;
	bool same_next_pixel = true;
	const char* dataPtr;
	bool raw = true;
	unsigned long chunk_start;
	unsigned long current_byte;

	for (unsigned long current_pixel=0; current_pixel < width * height; current_pixel += run_length) {
		chunk_start = current_pixel * channels;
		current_byte = current_pixel * channels;
		run_length = 1;
		raw = true;

		while ( current_pixel + run_length < npixels && run_length < max_chunk_length ) {

			for (int t=0; t < channels; t++) {
				if (!same_next_pixel){ break; }
				same_next_pixel = data[current_byte + t] == data[current_byte + t + channels];
			}

			current_byte += channels;

			if (run_length == 1) {
				raw = !same_next_pixel;
			}
			if (raw && same_next_pixel) {
				run_length--;
				break;
			}
			if (!raw && !same_next_pixel) {
				break;
			}
			run_length++;
		}
		dataPtr = (char *)(data + chunk_start);
		if (raw)
		{
			out.put(run_length - 1);
			out.write(dataPtr, run_length * channels );

		}else{
			out.put(run_length + 127);
			out.write(dataPtr, channels );
			for (int i = 0; i < channels; i++)
			{
				printf("%c", dataPtr[i]);
			}
			std::cout << std::endl;
		}
	}
}
