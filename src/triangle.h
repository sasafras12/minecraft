#ifndef _TRIANGLE_H
#define _TRIANGLE_H

#include <math.h>
#include "object.h"
#include "vec.h"
#include "colour.h"
#include "ray.h"

class Triangle : public Object {
	vec A, B, C;
	vec normal;
	double distance;
	Colour colour;

public:
	Triangle();
	Triangle(vec, vec, vec, Colour);

	// method functions
	vec getTriangleNormal() {
		vec CA (C.getVecX() - A.getVecX(), C.getVecY() - A.getVecY(), C.getVecZ() - A.getVecZ());
		vec BA (B.getVecX() - A.getVecX(), B.getVecY() - A.getVecY(), B.getVecZ() - A.getVecZ());
		normal = CA.crossProduct(BA).normalize();
		return normal;
	}
	double getTriangleDistance() {
		normal = getTriangleNormal();
		distance = normal.dotProduct(A);
		return distance;
	};
	virtual Colour getColour() { return colour; }

	virtual vec getNormalAt(vec point) {
		normal = getTriangleNormal();
		return normal;
	}

	virtual double findIntersection(Ray ray) {
		vec ray_direction = ray.getRayDirection();
		vec ray_origin = ray.getRayOrigin();

		normal = getTriangleNormal();
		distance = getTriangleDistance();
		double a = ray_direction.dotProduct(normal);
		
		if (a == 0) {
			// ray is parallel to the plane
			return -1;
		} else {
			double b = normal.dotProduct(ray.getRayOrigin().vecAdd(normal.vecMul(distance).negative()));
			double distance2plane = -1*b/a;

			double Qx = ray_direction.vecMul(distance2plane).getVecX() + ray_origin.getVecX();
			double Qy = ray_direction.vecMul(distance2plane).getVecY() + ray_origin.getVecY();
			double Qz = ray_direction.vecMul(distance2plane).getVecZ() + ray_origin.getVecZ();

			vec Q(Qx, Qy, Qz);
			
			// [CAxQA] * n >= 0
			vec CA (C.getVecX() - A.getVecX(), C.getVecY() - A.getVecY(), C.getVecZ() - A.getVecZ());
			vec QA (Q.getVecX() - A.getVecX(), Q.getVecY() - A.getVecY(), Q.getVecZ() - A.getVecZ());
			double test1 = (CA.crossProduct(QA)).dotProduct(normal);
			// [BCxQC] * n >= 0
			vec BC (B.getVecX() - C.getVecX(), B.getVecY() - C.getVecY(), B.getVecZ() - C.getVecZ());
			vec QC (Q.getVecX() - C.getVecX(), Q.getVecY() - C.getVecY(), Q.getVecZ() - C.getVecZ());
			double test2 = (BC.crossProduct(QC)).dotProduct(normal);
			// [ABxQB] * n >= 0
			vec AB (A.getVecX() - B.getVecX(), A.getVecY() - B.getVecY(), A.getVecZ() - B.getVecZ());
			vec QB (Q.getVecX() - B.getVecX(), Q.getVecY() - B.getVecY(), Q.getVecZ() - B.getVecZ());
			double test3 = (AB.crossProduct(QB)).dotProduct(normal);

			if ((test1 > 0) && (test2 > 0) && (test3 > 0)) {
				// inside triangle
				return -1 * b/a;
			} else {
				// outside triangle
				return -1;
			}

		}
	}

};

Triangle::Triangle() {
	A = vec(1, 0, 0);
	B = vec(0, 1, 0);
	C = vec(0, 0, 1);
	colour = Colour(0.5, 0.5, 0.5, 0);
}

Triangle::Triangle(vec pointA, vec pointB, vec pointC, Colour colourValue) {
	A = pointA;
	B = pointB;
	C = pointC;
	colour = colourValue;
}

#endif
