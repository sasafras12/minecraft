import pygame
import math
from math import cos, sin

pygame.init()
width = 400
height = 400
cx = width // 2
cy = height // 2
screen = pygame.display.set_mode((width, height))
running = True
white = (255,255,255)
black = (0,0,0)
clock = pygame.time.Clock()

points = (-1,-1,1),(1,-1,1),(1,1,1),(-1,1,1),(-1,-1,-1),(1,-1,-1),(1,1,-1),(-1,1,-1)
edges = (0,1),(1,2),(2,3),(3,0),(4,5),(5,6),(6,7),(7,4),(0,4),(1,5),(2,6),(3,7)

theta = 0
mul = 20
f1 = 50
f2 = 200
f3 = 100

while running:
	screen.fill(black)

	if (theta != 360):
		pass
		#theta +=1
	else:
		theta = 1
	rTheta = math.pi / 180 * theta
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False
	key = pygame.key.get_pressed()

	if key[pygame.K_a]:
		theta += 1


	if key[pygame.K_d]:
		theta -= 1

	p1 = (f3 + cx * (sin(rTheta)), f1 + mul * cos(rTheta))
	p2 = (f3 - cx * (sin(rTheta)), f2 + mul * cos(rTheta))
	p3 = (f3 - cx * (sin(rTheta)), f1 - mul * cos(rTheta))
	p4 = (f3 + cx * (sin(rTheta)), f2 - mul * cos(rTheta))
	p5 = (f3 - cx * (cos(rTheta)), f1 + mul * sin(rTheta))
	p6 = (f3 + cx * (cos(rTheta)), f2 + mul * sin(rTheta))
	p7 = (f3 + cx * (cos(rTheta)), f1 - mul * sin(rTheta))
	p8 = (f3 - cx * (cos(rTheta)), f2 - mul * sin(rTheta))

	pygame.draw.line(screen, white, (int(p2[0]), int(p2[1])),(int(p3[0]), int(p3[1])))
	pygame.draw.line(screen, white, (int(p1[0]), int(p1[1])),(int(p4[0]), int(p4[1])))
	pygame.draw.line(screen, white, (int(p7[0]), int(p7[1])),(int(p6[0]), int(p6[1])))
	pygame.draw.line(screen, white, (int(p8[0]), int(p8[1])),(int(p5[0]), int(p5[1])))
	pygame.draw.line(screen, white, (int(p1[0]), int(p1[1])),(int(p5[0]), int(p5[1])))
	pygame.draw.line(screen, white, (int(p1[0]), int(p1[1])),(int(p7[0]), int(p7[1])))
	pygame.draw.line(screen, white, (int(p3[0]), int(p3[1])),(int(p7[0]), int(p7[1])))
	pygame.draw.line(screen, white, (int(p3[0]), int(p3[1])),(int(p5[0]), int(p5[1])))
	pygame.draw.line(screen, white, (int(p2[0]), int(p2[1])),(int(p8[0]), int(p8[1])))
	pygame.draw.line(screen, white, (int(p4[0]), int(p4[1])),(int(p8[0]), int(p8[1])))
	pygame.draw.line(screen, white, (int(p4[0]), int(p4[1])),(int(p6[0]), int(p6[1])))
	pygame.draw.line(screen, white, (int(p2[0]), int(p2[1])),(int(p6[0]), int(p6[1])))




	pygame.display.flip()
	clock.tick(60)
