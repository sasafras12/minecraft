//g++ gtk_bindings.cpp -g `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0` -O2 -lpng -o gtk_bindings 

#include <gtk/gtk.h>
#include "maths.h"
#include <math.h>
#include <algorithm>
#include <list>
#include <fstream>

GtkWidget *window, *box;
GtkWidget *vbox;
GtkWidget *menuBar;
GtkWidget *fileMenu;
GtkWidget *fileMi;
GtkWidget *quitMi;
const int width = 1600, height = 900;
const char *file;
const char *obj_file;
GdkPixbuf *pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, width, height);
int rowstride = gdk_pixbuf_get_rowstride (pixbuf);
guchar *pixels = gdk_pixbuf_get_pixels (pixbuf);
GtkImage *image;

mesh meshCube;
mat4x4 matProj;
mat4x4 matRotZ, matRotX;
mat4x4 matTrans;
mat4x4 matWorld;
mat4x4 matCameraRotY;
mat4x4 matCameraRotX;
mat4x4 matCameraRot;
mat4x4 matCamera;
mat4x4 matView;

double fTheta = 0;
double fYaw;
double fPitch;
float z = 2.0f;
vec3d vCamera;
vec3d vLookDir;
vec3d vForward;
vec3d vLeft = {1, 0, 0};
vec3d vRight = {-1, 0, 0};
vec3d light_direction = {-7, 10, -10 };
vec3d vUp = { 0,1,0 };
vec3d vTarget = { 0,0,0 };


Sprite s;
float *pDepthBuffer = nullptr;
int SPRITE_SIZE_W;
int SPRITE_SIZE_H;

static gboolean is_w_pressed = FALSE;
static gboolean is_a_pressed = FALSE;
static gboolean is_s_pressed = FALSE;
static gboolean is_d_pressed = FALSE;
static gboolean is_q_pressed = FALSE;
static gboolean is_e_pressed = FALSE;


long int old_time = 0;

float last_position = 0;

std::vector<triangle> allTriangles;

static gboolean mouse_scroll(GtkWidget *widget, GdkEventScroll *event, gpointer user_data)
{
	std::cout << "Scrolled!" << std::endl;
	return TRUE;
}

static gboolean mouse_moved(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	
	if (event->type == GDK_MOTION_NOTIFY) {
		GdkWindow *win = gtk_widget_get_window(GTK_WIDGET(window));
		GdkScreen *screen = gdk_window_get_screen(win);
		GdkDisplay *display = gdk_window_get_display(win);
		GdkEventMotion *e = (GdkEventMotion*)event;
		if ((float)e->x >= width-1) {
			gdk_display_warp_pointer(display, screen, 0, (guint)e->y);	
			last_position = 0;
		}
		else if ((float)e->x == 0.0f) {
			gdk_display_warp_pointer(display, screen, width, (guint)e->y);	
			last_position = width;
		} else {
			float current_position = (float)e->x;
			float change_in_position = current_position - last_position;
			fYaw -= -change_in_position/width;
			last_position = current_position;
			fPitch = (float)e->y/height;
		}
	}
}

gboolean my_keypress_function (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	if (event->keyval == GDK_KEY_s) {
		is_s_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_w) {
		is_w_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_d) {
		is_d_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_a) {
		is_a_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_q) {
		is_q_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_e) {
		is_e_pressed = TRUE;
	}
	return GDK_EVENT_PROPAGATE;
}

static gboolean on_key_release(GtkWidget *widget, GdkEventKey *event)
{
	if (event->keyval == GDK_KEY_s) {
		is_s_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_w) {
		is_w_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_d) {
		is_d_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_a) {
		is_a_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_q) {
		is_q_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_e) {
		is_e_pressed = FALSE;
	}
	return GDK_EVENT_PROPAGATE;
}

inline void drawPixel (int x, int y, char red, char green, char blue)
{
    *(pixels + y * rowstride + x * 3) = red;
    *(pixels + y * rowstride + x * 3 + 1) = green;
    *(pixels + y * rowstride + x * 3 + 2) = blue;
}

void drawLine(int x1, int y1, int x2, int y2, int r, int g, int b)
	{
		int x, y, dx, dy, dx1, dy1, px, py, xe, ye, i;
		dx = x2 - x1; dy = y2 - y1;
		dx1 = abs(dx); dy1 = abs(dy);
		px = 2 * dy1 - dx1;	py = 2 * dx1 - dy1;
		if (dy1 <= dx1)
		{
			if (dx >= 0)
				{ x = x1; y = y1; xe = x2; }
			else
				{ x = x2; y = y2; xe = x1;}

			drawPixel(x, y, r, g, b);
			
			for (i = 0; x<xe; i++)
			{
				x = x + 1;
				if (px<0)
					px = px + 2 * dy1;
				else
				{
					if ((dx<0 && dy<0) || (dx>0 && dy>0)) y = y + 1; else y = y - 1;
					px = px + 2 * (dy1 - dx1);
				}
				drawPixel(x, y, r, g, b);
			}
		}
		else
		{
			if (dy >= 0)
				{ x = x1; y = y1; ye = y2; }
			else
				{ x = x2; y = y2; ye = y1; }

			drawPixel(x, y, r, g, b);

			for (i = 0; y<ye; i++)
			{
				y = y + 1;
				if (py <= 0)
					py = py + 2 * dx1;
				else
				{
					if ((dx<0 && dy<0) || (dx>0 && dy>0)) x = x + 1; else x = x - 1;
					py = py + 2 * (dx1 - dy1);
				}
				drawPixel(x, y, r, g, b);
			}
		}
}

void onCreate()
{
		pDepthBuffer = new float[width * height];
		s.read_png_file(file);

		matWorld = Matrix_MakeIdentity();
		meshCube.LoadFromObjectFile(obj_file, true);
		matProj = Matrix_MakeProjection(45.0f, (float)height / (float)width, 0.1f, 1000.0f);		

}

void TexturedTriangle(int x1, int y1, float u1, float v1, float w1, int x2, int y2, float u2, float v2, float w2, int x3, int y3, float u3, float v3, float w3, float shading) {

		if (y2 < y1)
		{
			std::swap(y1, y2);
			std::swap(x1, x2);
			std::swap(u1, u2);
			std::swap(v1, v2);
			std::swap(w1, w2);
		}

		if (y3 < y1)
		{
			std::swap(y1, y3);
			std::swap(x1, x3);
			std::swap(u1, u3);
			std::swap(v1, v3);
			std::swap(w1, w3);
		}

		if (y3 < y2)
		{
			std::swap(y2, y3);
			std::swap(x2, x3);
			std::swap(u2, u3);
			std::swap(v2, v3);
			std::swap(w2, w3);
		}

		int dy1 = y2 - y1;
		int dx1 = x2 - x1;
		float dv1 = v2 - v1;
		float du1 = u2 - u1;
		float dw1 = w2 - w1;

		int dy2 = y3 - y1;
		int dx2 = x3 - x1;
		float dv2 = v3 - v1;
		float du2 = u3 - u1;
		float dw2 = w3 - w1;

		float tex_u, tex_v, tex_w;

		float dax_step = 0, dbx_step = 0,
			du1_step = 0, dv1_step = 0,
			du2_step = 0, dv2_step = 0,
			dw1_step = 0, dw2_step = 0;

		if (dy1) dax_step = dx1 / (float)abs(dy1);
		if (dy2) dbx_step = dx2 / (float)abs(dy2);

		if (dy1) du1_step = du1 / (float)abs(dy1);
		if (dy1) dv1_step = dv1 / (float)abs(dy1);
		if (dy1) dw1_step = dw1 / (float)abs(dy1);

		if (dy2) du2_step = du2 / (float)abs(dy2);
		if (dy2) dv2_step = dv2 / (float)abs(dy2);
		if (dy2) dw2_step = dw2 / (float)abs(dy2);

		if (dy1)
		{
			for (int i = y1; i <= y2; i++)
			{
				int ax = x1 + (float)(i - y1) * dax_step;
				int bx = x1 + (float)(i - y1) * dbx_step;

				float tex_su = u1 + (float)(i - y1) * du1_step;
				float tex_sv = v1 + (float)(i - y1) * dv1_step;
				float tex_sw = w1 + (float)(i - y1) * dw1_step;

				float tex_eu = u1 + (float)(i - y1) * du2_step;
				float tex_ev = v1 + (float)(i - y1) * dv2_step;
				float tex_ew = w1 + (float)(i - y1) * dw2_step;

				if (ax > bx)
				{
					std::swap(ax, bx);
					std::swap(tex_su, tex_eu);
					std::swap(tex_sv, tex_ev);
					std::swap(tex_sw, tex_ew);
				}

				tex_u = tex_su;
				tex_v = tex_sv;
				tex_w = tex_sw;

				float tstep = 1.0f / ((float)(bx - ax));
				float t = 0.0f;

				for (int j = ax; j < bx; j++)
				{
					tex_u = (1.0f - t) * tex_su + t * tex_eu;
					tex_v = (1.0f - t) * tex_sv + t * tex_ev;
					tex_w = (1.0f - t) * tex_sw + t * tex_ew;
					if (tex_w > pDepthBuffer[i*width + j]) {
						int *col = s.getColour((int)((SPRITE_SIZE_W-1)*tex_u/tex_w), (int)((SPRITE_SIZE_H-1)*tex_v/tex_w));
						drawPixel(j, i, col[0]*shading, col[1]*shading, col[2]*shading);
						pDepthBuffer[i * width + j] = tex_w;
						t += tstep;
					}
				}

			}
		}

		dy1 = y3 - y2;
		dx1 = x3 - x2;
		dv1 = v3 - v2;
		du1 = u3 - u2;
		dw1 = w3 - w2;

		if (dy1) dax_step = dx1 / (float)abs(dy1);
		if (dy2) dbx_step = dx2 / (float)abs(dy2);


		du1_step = 0, dv1_step = 0, dw1_step = 0;
		if (dy1) du1_step = du1 / (float)abs(dy1);
		if (dy1) dv1_step = dv1 / (float)abs(dy1);
		if (dy1) dw1_step = dw1 / (float)abs(dy1);

		if (dy1)
		{
			for (int i = y2; i <= y3; i++)
			{
				int ax = x2 + (float)(i - y2) * dax_step;
				int bx = x1 + (float)(i - y1) * dbx_step;

				float tex_su = u2 + (float)(i - y2) * du1_step;
				float tex_sv = v2 + (float)(i - y2) * dv1_step;
				float tex_sw = w2 + (float)(i - y2) * dw1_step;

				float tex_eu = u1 + (float)(i - y1) * du2_step;
				float tex_ev = v1 + (float)(i - y1) * dv2_step;
				float tex_ew = w1 + (float)(i - y1) * dw2_step;

				if (ax > bx)
				{
					std::swap(ax, bx);
					std::swap(tex_su, tex_eu);
					std::swap(tex_sv, tex_ev);
					std::swap(tex_sw, tex_ew);
				}

				tex_u = tex_su;
				tex_v = tex_sv;
				tex_w = tex_sw;

				float tstep = 1.0f / ((float)(bx - ax));
				float t = 0.0f;

				for (int j = ax; j < bx; j++)
				{
					tex_u = (1.0f - t) * tex_su + t * tex_eu;
					tex_v = (1.0f - t) * tex_sv + t * tex_ev;
					tex_w = (1.0f - t) * tex_sw + t * tex_ew;
					if (tex_w > pDepthBuffer[i*width + j]) {
						int *col = s.getColour((int)((SPRITE_SIZE_W-1)*tex_u/tex_w), (int)((SPRITE_SIZE_H-1)*tex_v/tex_w));
						drawPixel(j, i, col[0]*shading, col[1]*shading, col[2]*shading);
						pDepthBuffer[i * width + j] = tex_w;
						t += tstep;
					}
				}
			}	
		}		
}

void drawModel(mat4x4 matWorld, mat4x4 matProj, mat4x4 matView, float x, float y, float z)
{
std::vector<triangle> vecTrianglesToRaster;
	for (auto tri : meshCube.tris)
	{
		triangle triProjected, triTransformed, triViewed;
		matWorld = Matrix_MakeTranslation(x, y, z);
		triTransformed.p[0] = Matrix_MultiplyVector(matWorld, tri.p[0]);
		triTransformed.p[1] = Matrix_MultiplyVector(matWorld, tri.p[1]);
		triTransformed.p[2] = Matrix_MultiplyVector(matWorld, tri.p[2]);

		triTransformed.t[0] = tri.t[0];
		triTransformed.t[1] = tri.t[1];
		triTransformed.t[2] = tri.t[2];

		// Calculate triangle normal
		vec3d normal, line1, line2;

		// Get lines either side of triangle
		line1 = Vector_Sub(triTransformed.p[1], triTransformed.p[0]);
		line2 = Vector_Sub(triTransformed.p[2], triTransformed.p[0]);

		// Take cross product of lines to get normal to triangle surface
		normal = Vector_CrossProduct(line1, line2);

		// You normally need to normalise a normal!
		normal = Vector_Normalise(normal);

		vec3d vCameraRay = Vector_Sub(triTransformed.p[0], vCamera);

		if (Vector_DotProduct(normal, vCameraRay) < 0.0f) {

			// Illumination

			light_direction = Vector_Normalise(light_direction);

			// how "aligned are light direction and triangle surface normal?
			float dp = std::max(0.1f, Vector_DotProduct(light_direction, normal));

			// Convert world space -> view space
			triViewed.p[0] = Matrix_MultiplyVector(matView, triTransformed.p[0]);
			triViewed.p[1] = Matrix_MultiplyVector(matView, triTransformed.p[1]);	
			triViewed.p[2] = Matrix_MultiplyVector(matView, triTransformed.p[2]);			
			triViewed.col = triTransformed.col;
			triViewed.t[0] = triTransformed.t[0];
			triViewed.t[1] = triTransformed.t[1];
			triViewed.t[2] = triTransformed.t[2];
			int nClippedTriangles = 0;
			triangle clipped[2];
			nClippedTriangles = Triangle_ClipAgainstPlane({ 0.0f, 0.0f, 0.1f }, { 0.0f, 0.0f, 1.0f }, triViewed, clipped[0], clipped[1]); 

			for (int n = 0; n < nClippedTriangles; n++)
			{

				// project triangles from 3D -> 2D
				triProjected.p[0] = Matrix_MultiplyVector(matProj, clipped[n].p[0]);
				triProjected.p[1] = Matrix_MultiplyVector(matProj, clipped[n].p[1]);
				triProjected.p[2] = Matrix_MultiplyVector(matProj, clipped[n].p[2]);
				triProjected.col = clipped[n].col;
				triProjected.t[0] = clipped[n].t[0];
				triProjected.t[1] = clipped[n].t[1];
				triProjected.t[2] = clipped[n].t[2];

				triProjected.t[0].u = triProjected.t[0].u / triProjected.p[0].w;
				triProjected.t[1].u = triProjected.t[1].u / triProjected.p[1].w;
				triProjected.t[2].u = triProjected.t[2].u / triProjected.p[2].w;
				
				triProjected.t[0].v = triProjected.t[0].v / triProjected.p[0].w;
				triProjected.t[1].v = triProjected.t[1].v / triProjected.p[1].w;
				triProjected.t[2].v = triProjected.t[2].v / triProjected.p[2].w;

				triProjected.t[0].w = 1.0f / triProjected.p[0].w;
				triProjected.t[1].w = 1.0f / triProjected.p[1].w;
				triProjected.t[2].w = 1.0f / triProjected.p[2].w;
				// scale into view

				triProjected.p[0] = Vector_Div(triProjected.p[0], triProjected.p[0].w);
				triProjected.p[1] = Vector_Div(triProjected.p[1], triProjected.p[1].w);
				triProjected.p[2] = Vector_Div(triProjected.p[2], triProjected.p[2].w);

				triProjected.p[0].x *= -1.0f;
				triProjected.p[1].x *= -1.0f;
				triProjected.p[2].x *= -1.0f;
				triProjected.p[0].y *= -1.0f;
				triProjected.p[1].y *= -1.0f;
				triProjected.p[2].y *= -1.0f;		

				vec3d vOffsetView = { 1, 1, 0 };

				triProjected.p[0] = Vector_Add(triProjected.p[0], vOffsetView);
				triProjected.p[1] = Vector_Add(triProjected.p[1], vOffsetView);
				triProjected.p[2] = Vector_Add(triProjected.p[2], vOffsetView);
				triProjected.p[0].x *= 0.5f * (float)width;
				triProjected.p[0].y *= 0.5f * (float)height;
				triProjected.p[1].x *= 0.5f * (float)width;
				triProjected.p[1].y *= 0.5f * (float)height;
				triProjected.p[2].x *= 0.5f * (float)width;
				triProjected.p[2].y *= 0.5f * (float)height;
				triProjected.col = dp;
				
				vecTrianglesToRaster.push_back(triProjected);
			}
		}
	}

	for (auto &triToRaster : vecTrianglesToRaster)
	{

		triangle clipped[2];
		std::list<triangle> listTriangles;
		listTriangles.push_back(triToRaster);
		int nNewTriangles = 1;

		for (int p = 0; p < 4; p++)
		{
			int nTrisToAdd = 0;
			while (nNewTriangles > 0)
			{
				triangle test = listTriangles.front();
				listTriangles.pop_front();
				nNewTriangles--;

				switch (p)
				{
					case 0:	nTrisToAdd = Triangle_ClipAgainstPlane({ 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, test, clipped[0], clipped[1]); break;
					case 1:	nTrisToAdd = Triangle_ClipAgainstPlane({ 0.0f, (float)height - 1, 0.0f }, { 0.0f, -1.0f, 0.0f }, test, clipped[0], clipped[1]); break;
					case 2:	nTrisToAdd = Triangle_ClipAgainstPlane({ 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f }, test, clipped[0], clipped[1]); break;
					case 3: nTrisToAdd = Triangle_ClipAgainstPlane({ (float)width - 1, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f }, test, clipped[0], clipped[1]); break;
				}
				for (int w = 0; w < nTrisToAdd; w++)
					listTriangles.push_back(clipped[w]);
			}
			nNewTriangles = listTriangles.size();
		}

		for (auto &t : listTriangles)
		{
			allTriangles.push_back(t);
		}
	}
}

gboolean game_loop (GtkWidget *widget, GdkFrameClock *clock, gpointer data)
{
	if (is_w_pressed) {
		vec3d vForward_ = {vForward.x, 0, vForward.z};
		vCamera = Vector_Add(vCamera, vForward_);
	}
	if (is_a_pressed) {
		vCamera = Vector_Sub(vCamera, vLeft);
	}
	if (is_s_pressed) {
		vec3d vForward_ = {vForward.x, 0, vForward.z};
		vCamera = Vector_Sub(vCamera, vForward_);
	}
	if (is_d_pressed) {
		vCamera = Vector_Sub(vCamera, vRight);
	}
	if (is_q_pressed) {
		vCamera.y -= 0.1f;
	}
	if (is_e_pressed) {
		vCamera.y += 0.1f;
	}

	gdk_pixbuf_fill(pixbuf, 0);
	for (int i = 0; i < width * height; i++) {
		pDepthBuffer[i] = 0.0f;
	}

	matRotZ = Matrix_MakeRotationZ(fTheta * 0.5f);
	matRotX = Matrix_MakeRotationX(fTheta);
	matTrans = Matrix_MakeTranslation(0.0f, 0.0f, 16.0f);
	matWorld = Matrix_MultiplyMatrix(matWorld, matTrans);
	vUp = { 0,1,0 };
	vTarget = { 0,0,1 };
	matCameraRotY = Matrix_MakeRotationY(fYaw);
	matCameraRotX = Matrix_MakeRotationX(fPitch);
	matCameraRot = Matrix_MultiplyMatrix(matCameraRotX, matCameraRotY);
	vLookDir = Matrix_MultiplyVector(matCameraRot, vTarget);

	vForward = Vector_Mul(vLookDir, 0.1f);
	vTarget = Vector_Add(vCamera, vLookDir);
	vLeft = Vector_CrossProduct(vLookDir, vUp);
	vLeft = Vector_Mul(vLeft, 0.1f);
	vRight = Vector_CrossProduct(vLookDir, vUp);
	vRight = Vector_Mul(vRight, -0.1f);
	matCamera = Matrix_PointAt(vCamera, vTarget, vUp);
	matView = Matrix_QuickInverse(matCamera);

	drawModel(matWorld, matProj, matView, 0, -3, 0);

	for (auto &t : allTriangles)
	{
		TexturedTriangle(t.p[0].x, t.p[0].y, t.t[0].u, t.t[0].v, t.t[0].w,
						t.p[1].x, t.p[1].y, t.t[1].u, t.t[1].v, t.t[1].w,
						t.p[2].x, t.p[2].y, t.t[2].u, t.t[2].v, t.t[2].w, t.col);
	}
	allTriangles.clear();
	// std::cout << vCamera.x << ", " << vCamera.y << ", " << vCamera.z << std::endl;
    drawPixel(width/2, height/2, 255, 255, 255);
    drawPixel(width/2-1, height/2, 255, 255, 255);
    drawPixel(width/2+1, height/2, 255, 255, 255);
    drawPixel(width/2, height/2-1, 255, 255, 255);
    drawPixel(width/2, height/2+1, 255, 255, 255);
    gtk_image_set_from_pixbuf (image, pixbuf);  
    return 1;
}

int main(int argc, char *argv[])
{
	if (argc != 5) {
		std::cout << "Error: ./model_viewer [/path/to/texture.png] [TEXTURE_WIDTH] [TEXTURE_HEIGHT] [/path/to/model.obj]" << std::endl;
		system("exit");
	}
    gtk_init (NULL, NULL);
	file = (const char*)argv[1];
	SPRITE_SIZE_W = std::atoi(argv[2]);
	SPRITE_SIZE_H = std::atoi(argv[3]);
	obj_file = (const char*)argv[4];
	onCreate();

    GtkWidget *container_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	vbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    menuBar = gtk_menu_bar_new();
    fileMenu = gtk_menu_new();
    fileMi = gtk_menu_item_new_with_label("File");
    quitMi = gtk_menu_item_new_with_label("Quit");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(fileMi), fileMenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(fileMenu), quitMi);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar), fileMi);
    gtk_box_pack_start(GTK_BOX(vbox), menuBar, TRUE, TRUE, 0);
    
    g_signal_connect(G_OBJECT(quitMi), "activate", G_CALLBACK(gtk_main_quit), NULL);

    image = GTK_IMAGE (gtk_image_new());
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
	gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);
	gtk_widget_add_events(GTK_WIDGET(container_box), GDK_SCROLL_MASK);
    box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start (GTK_BOX (box), GTK_WIDGET(image), TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(container_box), GTK_WIDGET(vbox), TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(container_box), GTK_WIDGET(box), TRUE, TRUE, 0);

    gtk_container_add (GTK_CONTAINER (window), container_box);
    g_signal_connect(G_OBJECT(container_box), "scroll-event", G_CALLBACK(mouse_scroll), NULL);
    g_signal_connect(G_OBJECT(window), "motion-notify-event", G_CALLBACK(mouse_moved), NULL);
	g_signal_connect (window, "key_press_event", G_CALLBACK (my_keypress_function), NULL);
	g_signal_connect (window, "key_release_event", G_CALLBACK (on_key_release), NULL);
	g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
    gtk_widget_add_tick_callback(window, game_loop, NULL, NULL);
	//gtk_window_fullscreen(GTK_WINDOW(window));
	GdkWindow *win = gtk_widget_get_window(GTK_WIDGET(window));
	gtk_widget_set_events(window, GDK_POINTER_MOTION_MASK);
	gtk_widget_show_all (window);
    gtk_main();

    delete[] pDepthBuffer;

}
