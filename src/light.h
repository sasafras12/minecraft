#ifndef _LIGHT_H
#define _LIGHT_H

#include "source.h"
#include "vec.h"
#include "colour.h"

class Light : public Source {
	vec position;
	Colour colour;

public:
	Light();
	Light(vec, Colour);

	// method functions
	virtual vec getLightPosition() { return position; }
	virtual Colour getLightColour() { return colour; }

};

Light::Light() {
	position = vec(0, 0, 0);
	colour = Colour(1, 1, 1, 0);
}

Light::Light(vec p, Colour c) {
	position = p;
	colour = c;
}

#endif
