#ifndef _CAMERA_H
#define _CAMERA_H

#include "vec.h"

class Camera {
	vec campos, camdir, camright, camdown;

public:
	Camera();
	Camera(vec, vec, vec, vec);

	// method functions
	vec getCameraPosition() { return campos; }
	vec getCameraDirection() { return camdir; }
	vec getCameraRight() { return camright; }
	vec getCameraDown() { return camdown; }

};

Camera::Camera() {
	campos = vec(0, 0, 0);
	camdir = vec(0, 0, 1);
	camright = vec(0, 0, 0);
	camdown = vec(0, 0, 0);
}

Camera::Camera(vec pos, vec dir, vec right, vec down) {
	campos = pos;
	camdir = dir;
	camright = right;
	camdown = down;
}

#endif
