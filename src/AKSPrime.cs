using System;
using System.IO;
using System.Numerics;
namespace AKSPrime
{
	public class AKSPrime
	{
		public static void Main(String[] args)
		{
			AKSPrime p = new AKSPrime();
			// TextReader k = new TextReader();
			Console.WriteLine("Input number for primality testing: ");
			int i = 1; // k.readInt();
			Console.WriteLine("Is " + i + " prime? ");
		}

		public bool isPrime(int numberToTest)
		{
			bool prime = true;
			bool flag = true;
			bool suitableQFound = false;
			bool polynomialsCongruent = true;
			int b, q;
			int suitableQ;
			double power;

			for (int a = 2; a <= Math.Sqrt(numberToTest); a++)
			{
				b = 2;
				power = Math.Pow(a, b);
				if (power == numberToTest)
				{
					prime = false;
					break;
				}
				b++;
			}
			// Algorithm Line 2
			int r = 2;
		
			// Algorithm Line 3
			while (r < numberToTest && flag && !suitableQFound)
			{
				// Algorithm Line 4
				if (GCD(numberToTest, r) != 1)
				{
					return false;
				}

				// Algorithm Line 5
				if (nonAKSisPrime(r))
				{
                	q = largestPrimeFactor(r - 1);
					double sqrtR = Math.Sqrt(r);
					double logN = Math.Log(numberToTest)/Math.Log(2);
                
					// Algorithm line 7
					if ( q >= 4*Math.Sqrt(r)*Math.Log(numberToTest)/Math.Log(2))
					{
						suitableQ = q;
						suitableQFound = true;
					}
				}
				
				// Algorithm line 9
				if (!suitableQFound) {
					r++;
				}

			}

			for (int a = 1; a <= 2 * Math.Sqrt(r) * Math.Log(numberToTest)/Math.Log(2); a++)
			{
				PolynomialArray poly1 = new PolynomialArray();
				poly1.binomialExpansionViaSquaring(-1 * a, numberToTest);

				int[] coeffs2 = {1, numberToTest, -1 * a, 0};
				PolynomialArray poly2 = new PolynomialArray(coeffs2);

				poly1.subtract(poly2);

				polynomialsCongruent = poly1.specialMod(r, numberToTest);
				if (!polynomialsCongruent) {
					prime = false;
				}
			}
			return prime;
		}

		private bool nonAKSisPrime(int x)
		{
			int f = 2;
			bool result = true;
			
			int s = (int)Math.Sqrt(x);
			
			while (f <= s && result)
			{
				if (x % f == 0)
				{
					result = false;
				}
				f++;
			}
			return result;
		}
		private static int GCD(int u, int v){
			BigInteger bigU = new BigInteger(u);
			BigInteger bigV = new BigInteger(v);
			int gcd = (int)BigInteger.GreatestCommonDivisor(bigU, bigV);
			return gcd;
		}

		private static int largestPrimeFactor(int input)
		{
			int f = 1;
			int r = 2;
			int x = input;
			
			while (x != 1 && r * r <= input)
			{
				while (x % r == 0)
				{
					x = x / r;
					f = r;
				}
				r++;
			}
			if (x == 1)
			{
				return f;
			} else {
				return x;
			}
		}
	}
}
