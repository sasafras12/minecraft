#ifndef _SOURCE_H
#define _SOURCE_H

class Source {
public:
	Source();

	virtual vec getLightPosition() { vec (0, 0, 0); };
	virtual Colour getLightColour() { return Colour(1, 1, 1,0);};
};

Source::Source() {};

#endif
