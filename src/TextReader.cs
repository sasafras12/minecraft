using System.IO;
public class TextReader
{
	private PushbackReader input;	// the input stream
	private bool rePrompting;	// users should be prompted, but not files
	
	public TextReader()
	{
		input = new PushbackReader( new InputStreamReader( System.in));
		rePrompting = true;
	}
	public TextReader (String fileName)
	{
		try {
			input = new PushbackReader(new FileReader(fileName));
			rePrompting = false;
		} catch (Exception e) {
			Console.WriteLine("can't open input file" + fileName ", program terminated");
			System.exit();
		}
	}
	private void error( String where )
	{
		Console.WriteLine("\n***Failure in " + where + " message. Program terminated***");
		System.exit();
	}
	public bool readBoolean()
	{
		do {
			String torf = readWord();	
			if (torf.equalsIgnoreCase("true")) {
				return true;		
			} else if (torf.equalsIgnoreCase("false")) {
				return false;
			} else {
				Console.WriteLine(torf + " is not 'true' or 'false. try again");
			}
		} while (true);
	}

	public String readLine()
	{
		String result = "";
		try {
			do {
				int next = input.read();
				if (next == '\r') {
					continue;
				}
				if (next == -1 || next == '\n') {
					break;	
				}
				result += (char)next;
			}while(true);
		} catch (Exception e) {
			error("Readline");
		}
		return result;

	}
	public char readChar()
	{
		return read();	
	}

	public char read()
	{
		char result = ' ';
		try {
			result = (char)input.read();
			if (result == '\r') {
				result = (char)input.read();
			}
		} catch (Exception e) {
			Console.WriteLine("Failure in call on read method, program terminated.");
			Console.Exit();
		}

		return result;
	}

	public void unread(char ch)
	{
		try {
			input.unread((byte)ch);
		} catch (Exception e) {
			error ("unread");
		}
	}

	public char peek()
	{
		int next = 0;
		try {
			next = input.read();
		} catch (Exception e) {
			error("peek");
		}

		if (next != -1)
		{
			unread( (char)next );
		}

		return (char)next;
	}

	public String readWord()
	{
		String result = "";
		try {
			int next;
			do {
				next = input.read();
			} while (next != -1 && !Character.isWhiteSpace( (char)next);

			while (next != -1 && !Character.isWhiteSpace( (char)next
			{
				result += (char)next;
				next = input.read();
			}
			while (next != -1 && next != '\n' && Character.isWhitespace((char)next))
			{
				next = input.read();
			}
			if (next != -1 && next != '\n') {
				unread((char)next);
			}
		} catch (Exception e)
		{
			error("Readword");
		}
	}

	public int readInt()
	{
		int result = 0;
		do // keep on trying until a alid double is entered
		{
			try {
				result = Integer.parseInt(readWord());
				break; // result is good, jump out of loop down to return result;
			}
			catch (Exception e)
			{
				if (rePrompting) {
					Console.WriteLine("invalid integer. try again");
				} else {
					error("readInt");
					break;
				}
			}
		} while (true);

		return result;
	}

	public double readDouble()
	{
		double result = 0.0;
		do {
			// keep on trying until a valid double is entered
			try {
				result = new Double(readWord()).doubleValue();
				break;
			} catch (Exception e)
			{
				if (rePrompting) {
					Console.WriteLine("Invalid floating-point number. Try again.");
				} else {
					error("ReadDouble");
					break;	
				}
			}
		} while (true);
		return result;
	}
	public bool ready()
	{
		bool result = false;
		try {
			result = input.ready();
		} catch (IOException e) {
			error("ready");
		}
		return result;
	}

	public static void main(String[] args)
	{
		Console.WriteLine("Enter password:");
		string pass = "";
		TextReader k = new TextReader();
		while (true) {
			char ch = k.peek();
			if (ch == '\n')
			{
				break;		
			}
			pass += "" + ch;
		}
		Console.WriteLine(pass);
	}
}
